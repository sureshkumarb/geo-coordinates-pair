var models = require('../models/model');

// on routes that end in /userroutes
// ----------------------------------------------------

// create a db (accessed at POST http://localhost:8080/userroute)
exports.savetoDB = function(req, res) {

    console.log(req.body);

    // create a new instance of the User model
    var model = new models();

    var lat1 = Number(req.body.startPoint[0]);
    var lon1 = Number(req.body.startPoint[1]);
    var lat2 = Number(req.body.endPoint[0]);
    var lon2 = Number(req.body.endPoint[1]);

    toRad = function (value) {
        return value * Math.PI / 180;
    }

    Haversine = function(lat1, lat2, lon1, lon2) {
        var R = 6371; // earth radius in km
        var dLat = toRad(lat2-lat1);
        var dLon = toRad(lon2-lon1);
        var lat1 = toRad(lat1);
        var lat2 = toRad(lat2);

        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) *
            Math.cos(lat1) * Math.cos(lat2);

        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return R * c;
    };

    var distance = Haversine(lat1, lat2, lon1, lon2);

    console.log("distance is " + distance);

    if(distance < 5) {

        var model = new models();

        // set the points information (comes from the request)
        model.startPoint = req.body.startPoint;
        model.endPoint = req.body.endPoint;
        model.distance = distance

        // save the data and check for errors
        models.create(model, function (err) {
            if (err) {
                console.log(err);
            }
            res.json({
                message: 'Successfully saved!',
                distance: distance
            });
        });
    }
    else {
        console.log('distance greater than 5km');
        res.json({
            message: 'distance greater than 5km',
            distance: distance
        })
    }
};


// get all the data (accessed at GET http://localhost:8080/userroute)
exports.displayAll = function(req, res) {
    models.find(function(err, data) {
        if(err) res.send(err);

        //return the data
        res.json(data);
    });
};

exports.removefromDB = function(req, res) {
    console.log(req.params);
    models.remove({
        _id: req.params.id
    }, function(err, user) {
        if(err) return res.send(err);

        res.json({ message: 'Successfully deleted'});
    });
};
