// grab the packages that we need for the user model
var mongoose = require('mongoose');
var schema = mongoose.Schema;

// user schema
var dbSchema = new schema({
    startPoint: [Number],
    endPoint: [Number],
    distance: Number
});

//return the model
module.exports = mongoose.model('db', dbSchema);