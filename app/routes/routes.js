var controllers = require('../controllers/controller.js');


module.exports = function(app, express) {

    var acceptRoute = express.Router();

    acceptRoute.route('/userroute')
        .get(controllers.displayAll)
        .post(controllers.savetoDB);

    acceptRoute.route('/userroute/:id')
        .delete(controllers.removefromDB);

    return acceptRoute;
};