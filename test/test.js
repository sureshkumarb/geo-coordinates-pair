var supertest = require("supertest");
var should = require("should");

// This agent refers to PORT where program is runninng.

var server = supertest.agent("http://localhost:8080");

// UNIT test begin

describe("SAMPLE unit test",function() {

    it("checking get request",function(done){

        //calling get api
        server
            .get('/userroute')
            .expect("Content-type",/json/)
            .expect(200)
            .end(function(err,res){
                res.status.should.equal(200);
                res.should.be.json;
                /*res.body.message.should.equal('distance greater than 5km');*/
                /*res.body.message.should.equal('Successfully saved!');*/
                done();
            });
    });

    it("checking post request",function(done){

        //calling post api
        server
            .post('/userroute')
            .send({startPoint: [13.004926, 77.589517], endPoint: [13.023532, 77.550336]})
            .expect("Content-type",/json/)
            .expect(200)
            .end(function(err,res){
                res.status.should.equal(200);
                res.body.message.should.equal('Successfully saved!');
                done();
            });
    });

    it("checking post request",function(done){

        //calling post api
        server
            .post('/userroute')
            .send({startPoint: [13.024912, 77.546559], endPoint: [13.003337, 77.592350]})
            .expect("Content-type",/json/)
            .expect(200)
            .end(function(err,res){
                res.status.should.equal(200);
                res.body.message.should.equal('distance greater than 5km');
                done();
            });
    });

    it("checking remove request",function(done){

        //calling delete api
        server
            .del('/userroute/582869864171ab3368058f72' )
            .expect("Content-type",/json/)
            .expect(200)
            .end(function(err,res){
                res.status.should.equal(200);
                done();
            });
    });



    it("checking remove request",function(done){

        //calling delete api
        server
            .del('/userroute/582869864171ab3368058f721')
            .expect("Content-type",/json/)
            .expect(200)
            .end(function(err,res){
                res.status.should.equal(200);
                done();
            });
    });

});