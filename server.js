var express = require('express');
var mongoose = require('mongoose');
var app = express();
var bodyParser = require('body-parser'); // get body-parser
var morgan = require('morgan'); // used to see requests
var config = require('./config'); // connective configured here
var path = require('path');


mongoose.connect(config.database);

// APP CONFIGURATION --------------
// use body parser so we can grab information from POST requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// configure our app to handle CORS requests
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET', 'POST', 'DELETE', 'PUT', 'PATCH', 'OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization, api_key');
    next();
});

// log all request to the console
app.use(morgan('dev'));

// API ROUTES ------------------------
var router = require('./app/routes/routes')(app, express);

app.use('/', router);

// set the public folder to server public assets
app.use(express.static(__dirname + '/public'));

app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/app/views/index.html'));
});

//  And start the app on that interface (and port).
app.listen(config.port);
console.log("Server Running at " + config.port);