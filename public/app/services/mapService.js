angular.module('mapService', [])

    .factory('Map', function($http) {

        var mapFactory = {};

        //check coordinates and save data
        mapFactory.send = function(startPoint, endPoint) {
            console.log('in service')
            var coordinates = {startPoint: startPoint, endPoint: endPoint }
            return $http.post('/userroute', coordinates);
        };

        //retrieve database
        mapFactory.showAll = function() {
            console.log('in showall')
            return $http.get('/userroute');
        };

        //retrieve database
        mapFactory.delete = function(id) {
            console.log('in delete')
            return $http.delete('/userroute/'+ id);
        };

        return mapFactory;

    });