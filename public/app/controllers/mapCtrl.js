var data = {
    lat: null,
    lng: null
};


angular.module('mapCtrl', ['mapService'])

    .controller('mapController', ['$scope', 'Map', function($scope, Map) {

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 0, lng: 0},
            zoom: 12,
            center: new google.maps.LatLng(14.224376, 76.410643),
            styles: [{
                featureType: 'poi',
                stylers: [{ visibility: 'off' }]  // Turn off POI.
            },
                {
                    featureType: 'transit.station',
                    stylers: [{ visibility: 'off' }]  // Turn off bus, train stations etc.
                }],
            disableDoubleClickZoom: true,
            streetViewControl: false,
        });


        $scope.markers = [];

        var createMarker = function (data){

            var marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(data[0], data[1])
            });

            $scope.markers.push(marker);

        };

        var lineSymbol = {
            path: 'M 0,-1 0,1',
            strokeOpacity: 1,
            scale: 4
        };

        var createLine = function (startPoint, endPoint) {
            console.log(startPoint, endPoint)
            var line = new google.maps.Polyline({
                path: [
                    new google.maps.LatLng(startPoint[0], startPoint[1]),
                    new google.maps.LatLng(endPoint[0], endPoint[1])
                ],
                strokeOpacity: 0,
                icons: [{
                    icon: lineSymbol,
                    offset: '0',
                    repeat: '20px'
                }],
                map: map
            });
        }

        var point = 0;

        // Listen for clicks and add the location of the click to firebase.
        map.addListener('click', function(e) {
            data = [e.latLng.lat(),e.latLng.lng()];
            if(point == 0) {
                for (var i = 0; i < $scope.markers.length; i++) {
                    $scope.markers[i].setMap(null);
                }
                startPoint = data;
                document.getElementById('over_map1').value = data;
                createMarker(data);
                point = 1;
            }
            else {
                endPoint = data;
                document.getElementById('over_map2').value = data;
                createMarker(data);
                point = 0;
                Map.send(startPoint, endPoint)
                    .success(function(data) {
                        if(data.distance < 5) {
                            document.getElementById('startpoint0').value = startPoint[0];
                            document.getElementById('startpoint1').value = startPoint[1];
                            document.getElementById('endpoint0').value = endPoint[0];
                            document.getElementById('endpoint1').value = endPoint[1];
                            document.getElementById('distance').value = data.distance;
                            $scope.saved = ', Points saved'
                            $scope.if_data = false;
                            var ifr = document.getElementsByName('showAll')[0];
                            ifr.src = ifr.src;
                            createLine(startPoint, endPoint);
                        } else {
                            document.getElementById('startpoint0').value = startPoint[0];
                            document.getElementById('startpoint1').value = startPoint[1];
                            document.getElementById('endpoint0').value = endPoint[0];
                            document.getElementById('endpoint1').value = endPoint[1];
                            document.getElementById('distance').value = data.distance;
                            $scope.saved = ', Not saved (distance > 5)'
                        }
                    })

                // Define a symbol using SVG path notation, with an opacity of 1.

            }
        });




    }])

    .controller('mapShowAllController', ['$scope', 'Map', function($scope, Map) {

        Map.showAll()
            .success(function(data) {
                if(data.length!=0) $scope.if_data = true;
                $scope.data = data;
            })
        console.log(data);

        // function to delete a points
        $scope.deleteLine = function(id) {

            $scope.if_data = false;

            // accepts the points id as a parameter
            Map.delete(id)
                .success(function(data) {
                    if(data.length!=0) $scope.if_data = true;

                    // get all points to update the table
                    // you can also set up your api
                    // to return the list of points with the delete call
                    Map.showAll()
                        .success(function(data) {
                            console.log(data);
                            $scope.data = data;
                        });
                });
        };

    }]);
