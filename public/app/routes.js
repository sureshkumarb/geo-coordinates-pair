angular.module('routerRoutes', ['ngRoute'])

    .config(function($routeProvider, $locationProvider) {

        $routeProvider

            .when('/', {
                templateUrl: 'app/views/pages/map.html',
                controller: 'mapController',
                controllerAs: 'map'
            })

            .when('/showall', {
                templateUrl: 'app/views/pages/showAll.html',
                controller: 'mapShowAllController',
                controllerAs: 'mapShow'
            });

        $locationProvider.html5Mode(true);
    });