angular.module('myApp', [
        'routerRoutes',
        'mapCtrl',
        'mapService'
    ])
    .run(["$rootScope", function($rootScope) {
        $rootScope.$on("$viewContentLoaded", function() {
            componentHandler.upgradeAllRegistered();
        });
    }]);
